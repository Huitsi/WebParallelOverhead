<!--
Copyright © 2020-2024 Linus Vanas <linus@vanas.fi>
SPDX-License-Identifier: MIT
-->
# Parallel Overhead (Web Edition)

![Screenshot](screenshot.png)

**WARNING: This game is visually intensive and has rapidly changing colors.**

Parallel Overhead is a colorful endless runner game where you take control of
the ships Truth and Beauty on a groundbreaking trip through hyperspace. A
stable hyperspace tunnel has finally been achieved with the two ships
supporting it on opposite walls of the tunnel. Well, almost stable...
It's up to you to keep the ships from falling through the cracks!

Parallel Overhead features
[Fast Pulse by oglsdl](https://opengameart.org/content/fast-pulse)
as its soundtrack.

## Links

* [Homepage](https://huitsi.net/WebParallelOverhead/)
* [Play in browser](https://play.huitsi.net/WebParallelOverhead/)
* [Project on Codeberg](https://codeberg.org/Huitsi/WebParallelOverhead)

## Controls

### Keyboard

* Space or enter to pause or unpause
* Left arrow key to move the ships left (clockwise rotation)
* Right arrow key to move the ships right (anticlockwise rotation)
* Backspace to restart

### Touch or mouse

* Tap/click in the middle to to pause or unpause
* Tap/click in the left quarter to move the ships left (clockwise rotation)
* Tap/click in the right quarter to move the ships right (anticlockwise rotation)

## Building from source

"Building" the game is simple: concatenate the JS-files in src/ into
parallel_overhead.js, or just run `make`. The files needed in a deployment are:

* data (the directory and its contents)
* index.html
* LICENSE.txt
* parallel_overhead.js
